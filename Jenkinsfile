pipeline {
	// Host Selection
    agent any
	
	// Install Environment Tools
	tools {
		maven 'Maven3'
		jdk 'JDK11'
	}
	
	options { timestamps() }
	
	// Autotrigger
	triggers{
    	bitbucketPush()
    }
    
    // Global Static Variables
    environment {
		NULL = ''
		DO_TEST = 'true'
		DO_BUILD = 'true'
		DO_SCAN = 'false'
		DO_PUBLISH = 'true'
		DO_DEPLOY = 'true'
		DO_REPORT_TO_JIRA = 'false'
		DOCKER_REGISTRY = 'nexus.acando.tech'
		DOCKER_REGISTRY_PUSHPORT = '5000'
		MAVEN_SETTINGS = 'acando-tech-maven-global-settings' //edit per Jenkins Instance
		DEPLOYMENT_PATH = '/opt/apps'
		// DEPLOYMENT_SERVER_ADDRESS = 'aiconnector1-dev.westeurope.cloudapp.azure.com'
		// JENKINS_KEY = 'jenkins.key'
    }
	
	// Define Pipeline Stages
    stages {
    	// Prepare
    	stage('Validate') {
    		when {
            	branch 'master'
            }
            steps {
            	cleanWs()
            	// Check out Code from GIT
                checkout scm
                
         		// Validate if project information is sufficient
        		sh 'mvn -f ${WORKSPACE}/pom.xml clean validate'
        		
               // load POM into environment for global access
        		script {
        			def pom = readMavenPom file: 'pom.xml'
        			env.POM = pom
        			env.VERSION = pom.version
        			env.ARTIFACT_ID  = pom.artifactId.toLowerCase()
        			env.GROUP_ID = pom.groupId
        			env.PACKAGING = pom.packaging
					/*     			
        			if (pom.version.contains('SNAPSHOT')) {
        				echo "### Version is SNAPSHOT - enabling CD Candidate"
        				env.CONTINUOUS_DELIVERY_CANDIDATE = 'true'
        			}
        			
        			else if (pom.version.contains('RELEASE')){
        				echo "### Version is RELEASE - disabling CD Candidate"
        				env.CONTINUOUS_DELIVERY_CANDIDATE = 'false'
        			}
        			*/    

        		}
        		echo "### VERSION: ${env.VERSION}"
        		echo "### ARTIFACT_ID: ${env.ARTIFACT_ID}"
        		echo "### GROUP_ID: ${env.GROUP_ID}"
        		echo "### PACKAGING: ${env.PACKAGING}"
            }
    	}
        // Unit Testing    
        stage('Test') {
            when {
            	branch 'master'
            	expression { env.DO_TEST == 'true' }
            }
            steps {
                sh 'mvn -f ${WORKSPACE}/pom.xml test'
            }
        }
        // Verify Build Package
        stage('Build') {
            when {
            	branch 'master'
            	expression { env.DO_BUILD == 'true' }
            }
            steps {
            	sh 'mvn -f ${WORKSPACE}/pom.xml verify'
            }
        }
        // Sonarqube Analysis
     	stage('Scan') {
     	    when {
     	    	branch 'master'
            	expression { env.DO_SCAN == 'true' }
            }
            steps {
                script {
                	scanner = tool name: 'SonarQubeScanner3', type: 'hudson.plugins.sonar.SonarRunnerInstallation'
                }
                withSonarQubeEnv('SonarQube') {
                    sh "${scanner}/bin/sonar-scanner -X"
                }
            }
        }
        // Deploy to Nexus
        stage('Publish') {
            when {	
            	branch 'master'
            	expression { env.DO_PUBLISH == 'true' }
            }
        	steps{
        		// Inject Nexus Address through global config
                withMaven(globalMavenSettingsConfig: "${env.MAVEN_SETTINGS}"){
                	sh 'mvn -f ${WORKSPACE}/pom.xml deploy'
                }
				script {
				echo "Building Docker Image"
				sh "docker build -f Dockerfile -t ${env.DOCKER_REGISTRY}:${env.DOCKER_REGISTRY_PUSHPORT}/${env.ARTIFACT_ID}:${env.VERSION} ."
				sh "docker tag ${env.DOCKER_REGISTRY}:${env.DOCKER_REGISTRY_PUSHPORT}/${env.ARTIFACT_ID}:${env.VERSION} ${env.DOCKER_REGISTRY}:${env.DOCKER_REGISTRY_PUSHPORT}/${env.ARTIFACT_ID}:latest"
				echo "Push Docker Images to Nexus"
				sh "docker push ${env.DOCKER_REGISTRY}:${env.DOCKER_REGISTRY_PUSHPORT}/${env.ARTIFACT_ID}:${env.VERSION}"
				sh "docker push ${env.DOCKER_REGISTRY}:${env.DOCKER_REGISTRY_PUSHPORT}/${env.ARTIFACT_ID}:latest"
				echo "Removing Docker Images"
				sh "docker image rm ${env.DOCKER_REGISTRY}:${env.DOCKER_REGISTRY_PUSHPORT}/${env.ARTIFACT_ID}:${env.VERSION}"
				sh "docker image rm ${env.DOCKER_REGISTRY}:${env.DOCKER_REGISTRY_PUSHPORT}/${env.ARTIFACT_ID}:latest"
				}
        	}
        }
        // Deploy to Hosts
        stage('Deployment') {
            when {
            	branch 'master'
            	expression { env.DO_DEPLOY == 'true' }
            }
            steps {
            	script{
            
			        withCredentials([	          
			          sshUserPrivateKey(credentialsId: "${JENKINS_KEY}", keyFileVariable: 'KEY', usernameVariable: 'SSH_USER')
			        ]){
    					echo '### Deploying artifact to target Server'
    					echo "${SSH_USER}" 
    					
    					try {
                            sh "ssh -i $KEY $SSH_USER@$DEPLOYMENT_SERVER_ADDRESS \"sudo su -c \'docker pull ${env.DOCKER_REGISTRY}/${env.ARTIFACT_ID}:latest\'; sudo systemctl --no-block restart docker-containers\""
                        } catch (error) {
                            echo "... could not deploy container"
                        }             	
                	}
				}
            }
       } 
       stage ('Post') {
       		when {
       			branch 'master'
       			expression { env.DO_REPORT_TO_JIRA == 'true' }
       		}
       		steps {
       			script {
       			// get Jira ID
       				def gitmessage = sh ( script: 'git log -n 1 --pretty=format:%s $GIT_COMMIT', returnStdout: true )
       				def ticketid = ''
       				try {
       					echo gitmessage
						ticketid = ( gitmessage =~ /([a-zA-Z][a-zA-Z0-9_]+-[1-9][0-9]*)([^.]|\.[^0-9]|\.$|$)/)[0][1]
       					echo "result: ${ticketid}"
       					withEnv(['JIRA_SITE=JIRA']) {
	                		def comment = "successfully run integration of: \"${env.ARTIFACT_ID}\" from branch: \"${BRANCH_NAME}\" - Job no. ${BUILD_NUMBER}"
		                	response = jiraAddComment idOrKey: ticketid, comment: comment
		    				echo response.successful.toString()
		    				echo response.data.toString()
	    				}	
					} catch (error) {
                		echo "${error} ... Commit Message does not contain a valid Jira Key/Issue - skipping step"
                	}		
				}
       		}
       }  
    }
}

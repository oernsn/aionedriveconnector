# Alpine Linux with OpenJDK JRE
FROM openjdk:11.0.2-jdk-slim-stretch
# copy JAR into image
COPY target/OneDriveConnector-*.jar OneDriveConnector.jar 
# run application with this command line 
CMD ["/usr/bin/java", "-jar", "-Dspring.profiles.active=default", "OneDriveConnector.jar"]

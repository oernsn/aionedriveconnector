package tech.acando.oneDriveConnector.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OneDriveHashesVO {

	String sha1Hash;
	String quickXorHash;
	
	public OneDriveHashesVO() {
		
	}

	public String getSha1Hash() {
		return sha1Hash;
	}

	public void setSha1Hash(String sha1Hash) {
		this.sha1Hash = sha1Hash;
	}

	public String getQickXorHash() {
		return quickXorHash;
	}

	public void setQickXorHash(String qickXorHash) {
		this.quickXorHash = qickXorHash;
	}
	
	
}

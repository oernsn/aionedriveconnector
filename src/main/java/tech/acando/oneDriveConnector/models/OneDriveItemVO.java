package tech.acando.oneDriveConnector.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OneDriveItemVO {

	String createdDateTime;
	String cTag;
	String eTag;
	String id;
	String lastModifiedDateTime;
	String name;
	int size;
	String webUrl;
	private OneDriveFolderVO folder;
	private OneDriveFileVO file;
	
	public OneDriveItemVO() {
		
	}

	public String getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(String createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public String getcTag() {
		return cTag;
	}

	public void setcTag(String cTag) {
		this.cTag = cTag;
	}

	public String geteTag() {
		return eTag;
	}

	public void seteTag(String eTag) {
		this.eTag = eTag;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLastModifiedDateTime() {
		return lastModifiedDateTime;
	}

	public void setLastModifiedDateTime(String lastModifiedDateTime) {
		this.lastModifiedDateTime = lastModifiedDateTime;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getWebUrl() {
		return webUrl;
	}

	public void setWebUrl(String webUrl) {
		this.webUrl = webUrl;
	}

	public OneDriveFolderVO getFolder() {
		return folder;
	}

	public void setFolder(OneDriveFolderVO folder) {
		this.folder = folder;
	}

	public OneDriveFileVO getFile() {
		return file;
	}

	public void setFile(OneDriveFileVO file) {
		this.file = file;
	}
	
	public boolean isFile() {
		if(this.file != null) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isFolder() {
		if(this.folder != null) {
			return true;
		} else {
			return false;
		}
	}
	
	

	
	
	
}

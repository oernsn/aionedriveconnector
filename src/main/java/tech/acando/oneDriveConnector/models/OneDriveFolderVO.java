package tech.acando.oneDriveConnector.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OneDriveFolderVO {

	int childCount;
	OneDriveViewVO view;
	
	public OneDriveFolderVO() {
		
	}

	public int getChildCount() {
		return childCount;
	}

	public void setChildCount(int childCount) {
		this.childCount = childCount;
	}

	public OneDriveViewVO getView() {
		return view;
	}

	public void setView(OneDriveViewVO view) {
		this.view = view;
	}
	
	

	
	
	
}

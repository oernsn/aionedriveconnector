package tech.acando.oneDriveConnector.models;

public class DiscoveryValueVO {
	
	String odata_type;
	String capability;
    String serviceApiVersion;
    String serviceEndpointUri;
    String serviceResourceId;

    public DiscoveryValueVO() {}

	public String getOdata_type() {
		return odata_type;
	}

	public void setOdata_type(String odata_type) {
		this.odata_type = odata_type;
	}

	public String getCapability() {
		return capability;
	}

	public void setCapability(String capability) {
		this.capability = capability;
	}

	public String getServiceApiVersion() {
		return serviceApiVersion;
	}

	public void setServiceApiVersion(String serviceApiVersion) {
		this.serviceApiVersion = serviceApiVersion;
	}

	public String getServiceEndpointUri() {
		return serviceEndpointUri;
	}

	public void setServiceEndpointUri(String serviceEndpointUri) {
		this.serviceEndpointUri = serviceEndpointUri;
	}

	public String getServiceResourceId() {
		return serviceResourceId;
	}

	public void setServiceResourceId(String serviceResourceId) {
		this.serviceResourceId = serviceResourceId;
	}
    
    
}

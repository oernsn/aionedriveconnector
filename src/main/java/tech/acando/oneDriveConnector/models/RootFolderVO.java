package tech.acando.oneDriveConnector.models;

import java.util.ArrayList;
import java.util.List;

public class RootFolderVO {

	public List<OneDriveItemVO> files;
	public List<OneDriveItemVO> folder;
	public List<OneDriveItemVO> unknowns;
	
	public RootFolderVO() {
		this.files = new ArrayList<OneDriveItemVO>();
		this.folder = new ArrayList<OneDriveItemVO>();
		this.unknowns = new ArrayList<OneDriveItemVO>();
	}

	public List<OneDriveItemVO> getFiles() {
		return files;
	}

	public void setFiles(List<OneDriveItemVO> files) {
		this.files = files;
	}

	public List<OneDriveItemVO> getFolder() {
		return folder;
	}

	public void setFolder(List<OneDriveItemVO> folder) {
		this.folder = folder;
	}

	public List<OneDriveItemVO> getUnknowns() {
		return unknowns;
	}

	public void setUnknowns(List<OneDriveItemVO> unknowns) {
		this.unknowns = unknowns;
	}
	
	
	

	
	
	
}

package tech.acando.oneDriveConnector.models;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ODataResponseVO {

	List<OneDriveItemVO> Value = new ArrayList<OneDriveItemVO>();

	public ODataResponseVO() {
		
	}
	
	public List<OneDriveItemVO> getValue() {
		return Value;
	}

	public void setValue(List<OneDriveItemVO> value) {
		Value = value;
	}
	
	
}

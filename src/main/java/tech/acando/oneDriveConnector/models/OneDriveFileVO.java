package tech.acando.oneDriveConnector.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OneDriveFileVO {

	String mimeType;
	OneDriveHashesVO hashes;
	
	public OneDriveFileVO() {
		
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public OneDriveHashesVO getHashes() {
		return hashes;
	}

	public void setHashes(OneDriveHashesVO hashes) {
		this.hashes = hashes;
	}
	
}

package tech.acando.oneDriveConnector.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import io.swagger.annotations.ApiOperation;
import tech.acando.oneDriveConnector.models.AccessTokenVO;
import tech.acando.oneDriveConnector.services.AuthService;

@RestController
@CrossOrigin
public class AuthController {

	@Autowired
	private AuthService authService;

	
	@ApiOperation(value = "Authentication with Microsoft Graph")
	@RequestMapping(method=RequestMethod.GET, value = "/authenticate/{clientId}")
	public RedirectView step1(@PathVariable String clientId) {
		return authService.getAuthorizationCodeGraph(clientId);
	}
	
	@ApiOperation(value = "Redirect")
	@RequestMapping(method=RequestMethod.GET, value = "/redirect")
	public AccessTokenVO step1redirect(@RequestParam String code) {
		if(authService.authStepTwo == true) {
			AccessTokenVO response = authService.getAAccessTokensGraph(code);
			return response;
		}
		return null;
	}
	
	
	
	
	
	
	
	
	
	
}

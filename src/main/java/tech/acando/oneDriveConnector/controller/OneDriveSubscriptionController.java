package tech.acando.oneDriveConnector.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import tech.acando.oneDriveConnector.models.OneDriveSubscriptionVO;
import tech.acando.oneDriveConnector.services.OneDriveSubscriptionService;
import tech.acando.oneDriveConnector.util.Util;

@RestController
@CrossOrigin
@RequestMapping("/subscribe")
public class OneDriveSubscriptionController {

	@Autowired
	private OneDriveSubscriptionService oneDriveSubscriptionService;
	
	@ApiOperation(value = "Create a subscription")
	@RequestMapping(method=RequestMethod.POST, value="/create")
	public String subscribe(@RequestBody OneDriveSubscriptionVO subscription) {
		OneDriveSubscriptionVO root = oneDriveSubscriptionService.createSubscription(subscription);
		String jsonString = Util.serializeJson(root);
		return jsonString;
	}
	
	@ApiOperation(value = "Validation ...")
	@RequestMapping(method=RequestMethod.POST, value="/subscribe", produces="application/text/plain")
	public String subscriptionUpdate(@RequestBody String anyString, @PathVariable String validationtoken) {
		String response = validationtoken;
		return response;
	}
	
	
	
	
	
	
	
	
	
	
}

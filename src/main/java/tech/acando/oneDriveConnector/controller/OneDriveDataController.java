package tech.acando.oneDriveConnector.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import tech.acando.oneDriveConnector.models.RootFolderVO;
import tech.acando.oneDriveConnector.services.OneDriveDataService;
import tech.acando.oneDriveConnector.util.Util;

@RestController
@CrossOrigin
@RequestMapping("/data")
public class OneDriveDataController {

	@Autowired
	private OneDriveDataService oneDriveDataService;
	
	@ApiOperation(value = "Get root directory for AiConnector.")
	@RequestMapping(method=RequestMethod.GET, value="/load")
	public String getRootFolder() {
		RootFolderVO root = oneDriveDataService.loadRootDirectory();
		String jsonString = Util.serializeJson(root);
		return jsonString;
	}
	
	@ApiOperation(value = "Get a directory by id.")
	@RequestMapping(method=RequestMethod.GET, value="/getDirectory/{id}")
	public void getFolder(@PathVariable String id) {
		RootFolderVO root = oneDriveDataService.getDirectoryItems(id);
		String json = Util.serializeJson(root);
		System.out.println(json);
	}
	
	
	
	
	
	
	
	
	
	
}

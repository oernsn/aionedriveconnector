package tech.acando.oneDriveConnector.services;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import tech.acando.oneDriveConnector.models.ODataResponseVO;
import tech.acando.oneDriveConnector.models.OneDriveItemVO;
import tech.acando.oneDriveConnector.models.RootFolderVO;

@Service
public class OneDriveDataService extends ServiceBase<OneDriveDataService> {

	public synchronized RootFolderVO loadRootDirectory() {
		
		String rootFolderId = this.getRootDirectoryId();
		
		if(rootFolderId == null) {
			System.out.println("Root folder not found!");
			return null;
		}
		
		RootFolderVO rootFolder = this.getDirectoryItems(rootFolderId); 
		
		return rootFolder;
	}

	private synchronized String getRootDirectoryId() {

		String url = "https://graph.microsoft.com/v1.0/me/drive/root/children";
		String authorization = "Bearer " + this.accessToken.getAccess_token();
		String rootFolderId = null;
		
		HttpClient httpclient = HttpClients.createDefault();

		

		try {
			URIBuilder builder = new URIBuilder(url);
			URI uri = builder.build();
			
			HttpGet request = new HttpGet(uri);
			request.setHeader("Authorization", authorization);

			HttpResponse res = httpclient.execute(request);
			HttpEntity entity = res.getEntity();

			var json = EntityUtils.toString(entity);
			var ODataresponse = new ObjectMapper().readValue(json, ODataResponseVO.class);
			var values = ODataresponse.getValue();

			List<OneDriveItemVO> unknowns = new ArrayList<OneDriveItemVO>();

			for (var o : values) {
				if (o.getClass() == OneDriveItemVO.class) {
					OneDriveItemVO item = (OneDriveItemVO) o;
					if (item.isFolder()) {
						String name = item.getName().toLowerCase().toString();
						if(name.equals("aiconnector")) {
							rootFolderId = item.getId();
						}
					} else {
						unknowns.add((OneDriveItemVO) o);
					}
				}
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return rootFolderId;
	}

	public synchronized RootFolderVO getDirectoryItems(String folderId) {
		
		String url = "https://graph.microsoft.com/v1.0/me/drive/items/" + folderId + "/children";

		String authorization = "Bearer " + this.accessToken.getAccess_token();

		HttpClient httpclient = HttpClients.createDefault();

		RootFolderVO rootFolder = new RootFolderVO();

		try {
			URIBuilder builder = new URIBuilder(url);

			URI uri = builder.build();

			HttpGet request = new HttpGet(uri);

			request.setHeader("Authorization", authorization);

			HttpResponse res = httpclient.execute(request);
			HttpEntity entity = res.getEntity();

			var json = EntityUtils.toString(entity);
			var ODataresponse = new ObjectMapper().readValue(json, ODataResponseVO.class);
			var values = ODataresponse.getValue();

			for (var o : values) {
				if (o.getClass() == OneDriveItemVO.class) {
					OneDriveItemVO item = (OneDriveItemVO) o;
					if (item.isFile()) {
						rootFolder.files.add(item);
					} else if (item.isFolder()) {
						rootFolder.folder.add(item);
					}
				} else {
					rootFolder.unknowns.add((OneDriveItemVO) o);
				}
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return rootFolder;
	}
	
	

}


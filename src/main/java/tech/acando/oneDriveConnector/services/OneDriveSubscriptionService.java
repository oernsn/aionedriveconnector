package tech.acando.oneDriveConnector.services;

import java.net.URI;

import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import tech.acando.oneDriveConnector.models.OneDriveSubscriptionVO;
import tech.acando.oneDriveConnector.util.Util;

@Service
public class OneDriveSubscriptionService extends ServiceBase<OneDriveSubscriptionService> {

	public synchronized OneDriveSubscriptionVO createSubscription(OneDriveSubscriptionVO subscription) {

		String url = "https://graph.microsoft.com/v1.0/me/subscriptions";

		String authorization = "Bearer " + this.accessToken.getAccess_token();

		HttpClient httpclient = HttpClients.createDefault();
		
		OneDriveSubscriptionVO responseSubscription = null;

		try {
			URIBuilder builder = new URIBuilder(url);
			URI uri = builder.build();

			HttpPost request = new HttpPost(uri);
			
			
			String serialisedJsonString = Util.serializeJson(subscription);	
			StringEntity entity = new StringEntity(serialisedJsonString);
			
			request.setEntity(entity);
			request.setHeader("Authorization", authorization);
			
			HttpEntity ResponseEntity = httpclient.execute(request).getEntity();

			var json = EntityUtils.toString(ResponseEntity);
			responseSubscription = new ObjectMapper().readValue(json, OneDriveSubscriptionVO.class);


		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return responseSubscription;
		
	}

	public synchronized void Subscriptionvalidation() {}
	
}


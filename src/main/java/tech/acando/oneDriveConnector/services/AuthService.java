package tech.acando.oneDriveConnector.services;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.view.RedirectView;

import tech.acando.oneDriveConnector.models.AccessTokenVO;

@Service
public class AuthService extends ServiceBase<AuthService> {
	
//	private final Logger logger = LoggerFactory.getLogger(AuthService.class);
	private String client_secret = "rrSNFU2089(uzsgpPGD1|?}";
	public boolean authStepTwo = false;
	
	// Authorization with Microsoft Graph
	// Step 1
	public synchronized RedirectView getAuthorizationCodeGraph(String clientId) {
		try {
		    RedirectView redirectView = new RedirectView();
		    
		    URIBuilder builder = new URIBuilder("https://login.microsoftonline.com/common/oauth2/v2.0/authorize");
			
		    List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("client_id", clientId));
			params.add(new BasicNameValuePair("scope","offline_access files.readwrite.all"));
			params.add(new BasicNameValuePair("response_type","code"));
			params.add(new BasicNameValuePair("redirect_uri","http://localhost:8585/api/redirect"));
			
			builder.addParameters(params);
	
			URI uri = builder.build();
			
		    redirectView.setUrl(uri.toString());
		    
		    this.authStepTwo = true;
		    
		    return redirectView;

		} catch(Exception e) {
			logger.error(e.getMessage());
			return null;
		}
	}
	// Step 2
	public synchronized AccessTokenVO getAAccessTokensGraph(String authCode) {

		String uri = "https://login.microsoftonline.com/common/oauth2/v2.0/token";
		AccessTokenVO token = null;
		List<NameValuePair> params = new ArrayList<NameValuePair>();

		try {
			params.add(new BasicNameValuePair("client_id", this.configuration.getClientId()));
			params.add(new BasicNameValuePair("redirect_uri", "http://localhost:8585/api/redirect"));
			params.add(new BasicNameValuePair("client_secret", this.client_secret));
			params.add(new BasicNameValuePair("code", authCode));
			params.add(new BasicNameValuePair("grant_type", "authorization_code"));

			HttpEntity entity = this.postRequest(uri, params).getEntity();

			if (entity != null) {
				token = gson.fromJson(EntityUtils.toString(entity), AccessTokenVO.class);
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		this.authStepTwo = false;

		this.accessToken = token;
		return token;
	}
	

}


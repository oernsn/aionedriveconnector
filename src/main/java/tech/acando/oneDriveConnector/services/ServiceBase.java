package tech.acando.oneDriveConnector.services;

import java.net.URI;
import java.util.AbstractMap;
import java.util.List;
import java.util.Set;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import tech.acando.oneDriveConnector.config.Configuration;
import tech.acando.oneDriveConnector.models.AccessTokenVO;

public abstract class ServiceBase<T> {
	
	public Gson gson = new Gson();
	AccessTokenVO accessToken;
	public Configuration configuration;
	
	public final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public ServiceBase() {
		this.configuration = Configuration.getInstance();
	}

//	public synchronized <T>T validateResponse(HttpResponse response, Class<T> type) {
//		if (response != null) {
//        	String status = Integer.toString(response.getStatusLine().getStatusCode());
//        	
//        	if(status.equals("200") || status.equals("202")) {
//        		HttpEntity entity = response.getEntity();
//   			 	if (entity != null) {
//   			 		try {
//   			 			T res = gson.fromJson(EntityUtils.toString(entity), type);
//   			 			return res;
//   			 		} catch (Exception e) {
//   			 			logger.error(e.getMessage());
//   			 		}
//   	            }
//        	} else{
//        		logger.error("Http call ended with error: " + response.getStatusLine().toString());
//        	}
//        }
//		return null;
//	}
//	
//	public synchronized <T>T validateResponse(HttpResponse response, Type type) {
//		if (response != null) {
//        	String status = Integer.toString(response.getStatusLine().getStatusCode());
//        	
//        	if(status.equals("200") || status.equals("202")) {
//        		HttpEntity entity = response.getEntity();
//   			 	if (entity != null) {
//   			 		try {
//   			 			T res = gson.fromJson(EntityUtils.toString(entity), type);
//   			 			return res;
//   			 		} catch (Exception e) {
//   			 			logger.error(e.getMessage());
//   			 		}
//   	            }
//        	} else{
//        		logger.error("Http call ended with error: " + response.getStatusLine().toString());
//        	}
//        }
//		return null;
//	}
	
	public synchronized HttpResponse postRequest (String _uri, List<NameValuePair> entity) {
		HttpClient httpclient = HttpClients.createDefault();
		try {
			URIBuilder builder = new URIBuilder(_uri);
						
			URI uri = builder.build();

			HttpPost request = new HttpPost(uri);
			
			request.setEntity(new UrlEncodedFormEntity(entity));
			request.setHeader("Content-Type", "application/x-www-form-urlencoded");
		
			return httpclient.execute(request);

		} catch (Exception e) {
			logger.error(e.getMessage());
			return null;
		}
		
	}
	
	public synchronized HttpResponse putRequest (String key, String _uri, Set<AbstractMap.SimpleEntry<String, String>> _header, HttpEntity _entity) {
		HttpClient httpclient = HttpClients.createDefault();
		try {
			URIBuilder builder = new URIBuilder(_uri);
			URI uri = builder.build();
			HttpPut request = new HttpPut(uri);
			
			_header.forEach(item -> {
				request.setHeader(item.getKey(), item.getValue());
			});
			
			request.setHeader("Ocp-Apim-Subscription-Key", key);
			
			request.setEntity(_entity);
			
			return httpclient.execute(request);

		} catch (Exception e) {
			logger.error(e.getMessage());
			return null;
		}
	}
	
	public synchronized HttpResponse getRequest (String _uri, List<NameValuePair> _params) {
		HttpClient httpclient = HttpClients.createDefault();
		try {
			URIBuilder builder = new URIBuilder(_uri);
			
			builder.addParameters(_params);

			URI uri = builder.build();

			HttpGet request = new HttpGet(uri);
			
			
			
			return httpclient.execute(request);

		} catch (Exception e) {
			logger.error(e.getMessage());
			return null;
		}
	}
	
	public synchronized HttpResponse getRequest (String _uri, Set<AbstractMap.SimpleEntry<String, String>> _header) {
		HttpClient httpclient = HttpClients.createDefault();
		try {
			URIBuilder builder = new URIBuilder(_uri);
			
			URI uri = builder.build();

			HttpGet request = new HttpGet(uri);
			
			return httpclient.execute(request);

		} catch (Exception e) {
			logger.error(e.getMessage());
			return null;
		}
	}
	
	public synchronized HttpResponse deleteRequest (String key, String _uri, Set<AbstractMap.SimpleEntry<String, String>> _header) {
		HttpClient httpclient = HttpClients.createDefault();
		try {
			URIBuilder builder = new URIBuilder(_uri);
			URI uri = builder.build();
			HttpDelete request = new HttpDelete(uri);
			
			if (_header != null) {
				_header.forEach(item -> {
					request.setHeader(item.getKey(), item.getValue());
				});
			}
			
			request.setHeader("Ocp-Apim-Subscription-Key", key);
			
			return httpclient.execute(request);

		} catch (Exception e) {
			logger.error(e.getMessage());
			return null;
		}
	}
}

package tech.acando.oneDriveConnector;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class Setup implements CommandLineRunner {
	
	@Override
	public void run(String... strings) throws Exception {	
		
		System.out.println("OneDrive Connector started...");
	}
}
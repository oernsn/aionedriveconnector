package tech.acando.oneDriveConnector.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Base64;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Util {
	
	public static <T>String serializeJson(T model) {
		Gson gson;
		GsonBuilder builder = new GsonBuilder(); 
	    builder.setPrettyPrinting(); 
	    gson = builder.create(); 
	    return gson.toJson(model);
	}
	
	public static <T>String serializeJson(T model, GsonBuilder builder) {
		Gson gson;
	    gson = builder.create(); 
	    return gson.toJson(model);
	}
	
	public static File Base64ToFile(String base64String, String fileType) {

		String filePath = "asset." + fileType;
		
		byte[] data = Base64.getDecoder().decode(base64String);

		try( OutputStream stream = new FileOutputStream(filePath) ) 
		{
		   stream.write(data);
		   return new File(filePath);
		}
		catch (Exception e) 
		{
		   System.err.println("Couldn't write to file...");
		   return null;
		}
		
	    
	}
}

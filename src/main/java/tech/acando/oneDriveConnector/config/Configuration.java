package tech.acando.oneDriveConnector.config;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Configuration {
	
	private final Logger logger = LoggerFactory.getLogger(Configuration.class);
	
	private final String propertiesPath = "./src/main/resources/onedrive.properties";
	private String clientId;
	private String secretKey;
	private String tenant;
	private String authority;
	private String fullAuthority;
	private String microsoftOnlineURI;

	private static Configuration INSTANCE;

	public static Configuration getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Configuration();
		}
		return INSTANCE;
	}
	
	private Configuration (){
		Properties properties = readProperties(propertiesPath);	
		try{
			this.clientId = properties.get("client.id").toString().trim();
			this.secretKey = properties.get("secret.key").toString().trim();
			this.authority  = properties.get("authority.uri").toString().trim();
			this.microsoftOnlineURI = properties.get("microsoftonline.com").toString().trim();
		} catch (Exception e) {
			logger.error("Not all Config parameters are set. Please correct the respective properties-file");
		}
	}
	
	
		
	
	public String getClientId() {
		return clientId;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public String getTenant() {
		return tenant;
	}

	public String getAuthority() {
		return authority;
	}

	public String getFullAuthority() {
		return fullAuthority;
	}
	
	public String getMicrosoftOnlineURI() {
		return microsoftOnlineURI;
	}

	private Properties readProperties(String filePath) {
		try {
			File propertiesFile = new File(filePath);
			Properties properties = null;
			if (propertiesFile.exists()) {
				properties = new Properties();
				BufferedInputStream bis = new BufferedInputStream(new FileInputStream(propertiesFile));
				properties.load(bis);
				bis.close();
			}
			return properties;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
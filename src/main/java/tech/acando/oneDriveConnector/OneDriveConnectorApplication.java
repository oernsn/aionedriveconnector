package tech.acando.oneDriveConnector;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OneDriveConnectorApplication {

	public static void main(String[] args) {
		SpringApplication.run(OneDriveConnectorApplication.class, args);
	}

}
